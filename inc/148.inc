    <div class="tip">
        The <code>\o filename</code> meta command will redirect all query results into the
	file specified.

        “Query results” includes all tables, command responses, and notices
	obtained from the database server, as well as output of various
	backslash commands that query the database (such as <code>\d</code>); but not error
	messages.
      <pre><code class="hljs bash">laetitia=# \o out.out
laetitia=# select * from test limit 5;
laetitia=# \! cat out.out
 id | value 
----+-------
  1 | bla
  2 | bla
  3 | bla
  4 | bla
  5 | bla
(5 rows)</code></pre>This feature is available
at least since Postgres 7.1.
	</div>

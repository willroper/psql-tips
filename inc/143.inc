    <div class="tip">The <code>\i filename</code> or <code>\include filename</code> metacommand will
    read the input from the file filename and execute it.
      <pre><code class="hljs bash">laetitia=# \! cat test.sql
select * from test;
laetitia=# \i test.sql
 id | value 
----+-------
  1 | bla
  2 | bla
  3 | bla
  4 | bla
  5 | bla
  6 | bla
(6 rows)</code></pre>This feature is available since
      at least Postgres 7.1.
	</div>

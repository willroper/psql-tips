    <div class="tip">The <code>\H</code> or <code>\html</code> metacommand will turn on the HTML query output
    format.
      <pre><code class="hljs bash">laetitia=# \H
Output format is html.
laetitia=# select *
laetitia-# from test
laetitia-# where id = 1;
<table border="1">
  <tr>
    <th align="center">id</th>
    <th align="center">value</th>
  </tr>
  <tr valign="top">
    <td align="right">1</td>
    <td align="left">bla</td>
  </tr>
</table>
<p>(1 row)<br />
</p></code></pre>This feature is available since
      at least Postgres 7.1.
	</div>

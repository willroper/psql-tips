
script_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd -P)

usage() {
  cat << EOF 
Usage: $(basename "${BASH_SOURCE[0]}") [-h] [-a|--isarg] option

This script will scan versions of the psql official documentation to determine
the first version where this option/argument was seen. By default, the script
assumes it's an option and will add the '\' in front of the option. If the '-a'
 flag is used, it will add the '-' or the '--' in front of the argument,
 depending on how many letters are in the argument.

Example:
  - $(basename "${BASH_SOURCE[0]}") g
    Will look for the first version where the meta command \g was introduced
  - $(basename "${BASH_SOURCE[0]}") -a expanded
    Will look for the first version where the option --expanded was introduced
  - $(basename "${BASH_SOURCE[0]}") -a x
    Will look for the first version where the option -x was introduced

Available options:

-h, --help      Print this help and exit
-a, --isarg       if the option given is an argument
EOF
  exit
}

cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
  # script cleanup here
}

setup_colors() {
  if [[ -t 2 ]] && [[ -z "${NO_COLOR-}" ]] && [[ "${TERM-}" != "dumb" ]]; then
    NOFORMAT='\033[0m' RED='\033[0;31m' GREEN='\033[0;32m' ORANGE='\033[0;33m' BLUE='\033[0;34m' PURPLE='\033[0;35m' CYAN='\033[0;36m' YELLOW='\033[1;33m'
  else
    NOFORMAT='' RED='' GREEN='' ORANGE='' BLUE='' PURPLE='' CYAN='' YELLOW=''
  fi
}

msg() {
  #echo >&2 -e "${1-}"
  echo "${1-}"
}

die() {
  local msg=$1
  local code=${2-1} # default exit status 1
  msg "$msg"
  exit "$code"
}

parse_params() {
  # default values of variables set from params
  isarg=false

  while :; do
    case "${1-}" in
    -h | --help) usage ;;
    -a | --isarg) isarg=true # Default is false
      param="${2-}"
      shift
      ;;
    -?*) die "Unknown option: $1" ;;
    *) break ;;
    esac
    shift
  done

  args=("$@")

  # check required params and arguments
  [[ ${#args[@]} -eq 0 ]] && die "Missing script arguments"

  return 0
}

parse_params "$@"
setup_colors

## script logic here

msg "${RED}Read parameters:${NOFORMAT}"
msg "- isarg: ${isarg}"
msg "- arguments: ${args[*]-}"

msg "creating the grep filter"
filter=""
if ${isarg}; then
  echo TODO;
else
  filter='<dt>(<span class=\"term\"><code class=\"literal\">|<tt class=\"LITERAL\">)\\\'${args[0]}
fi

msg "- filter: ${filter}"

versions=("7.1" "7.2" "7.3" "7.4" "8.0" "8.1" "8.2" "8.3" "8.4" "9.0" "9.1" "9.2" "9.3" "9.4" "9.5" "9.6" "10" "11" "12" "13" "14" "15")
found=false

for i in "${versions[@]}";
do
  msg "Parsing https://www.postgresql.org/docs/${i}/app-psql.html";
 if curl -s "https://www.postgresql.org/docs/${i}/app-psql.html" | egrep -q "$filter"; then
   msg "Argument \\${args[0]} found in version: ${GREEN}${i}${NOFORMAT} of the psql documentation";
   found=true;
   break;
 fi

done

if ! ${found}; then
  msg "Argument \\${args[0]} not found in ${RED}any version${NOFORMAT} of the psql documentation";
  msg "Check your argument."
fi

